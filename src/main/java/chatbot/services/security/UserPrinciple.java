package chatbot.services.security;

import com.fasterxml.jackson.annotation.JsonIgnore;

import chatbot.services.beans.User;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
 
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
 
public class UserPrinciple implements UserDetails {
  private static final long serialVersionUID = 1L;
 
  	private Long id;
    private String login;
    private String email;
    @JsonIgnore
    private String password;
 
    private GrantedAuthority authority;
 
    public UserPrinciple(Long id,
              String login, String email, String password, 
              GrantedAuthority authority) {
        this.id = id;
        this.login = login;
        this.email = email;
        this.password = password;
        this.authority = authority;
    }
 
	public static UserPrinciple build(User user) {
    	GrantedAuthority  authority=new SimpleGrantedAuthority(user.getProfil().getProfil());
        /*List<GrantedAuthority> authorities = user.getProfil().stream().map(role ->
                new SimpleGrantedAuthority(role.getName().name())
        ).collect(Collectors.toList());*/
        return new UserPrinciple(
                user.getId_utilisateur(),
                user.getLogin(),
                user.getEmail(),
                user.getMdp(),
                authority
        );
    }
 
    public Long getId() {
        return id;
    }
 
 
 
    public String getEmail() {
        return email;
    }
 
    @Override
    public String getUsername() {
        return login;
    }
 
    @Override
    public String getPassword() {
        return password;
    }
 
   
 
    public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public GrantedAuthority getAuthority() {
		return authority;
	}

	@Override
    public boolean isAccountNonExpired() {
        return true;
    }
 
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
 
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
 
    @Override
    public boolean isEnabled() {
        return true;
    }
 
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        UserPrinciple user = (UserPrinciple) o;
        return Objects.equals(id, user.id);
    }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> list = new LinkedList<GrantedAuthority>(); 
		list.add(getAuthority());
		return list;
	}
}
