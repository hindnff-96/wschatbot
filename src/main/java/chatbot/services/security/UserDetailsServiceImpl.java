package chatbot.services.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import chatbot.services.beans.User;
 
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
 
    @Autowired
    chatbot.services.dao.UserRepository userRepository;
 
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
      
        User user = userRepository.findByLogin(username)
                  .orElseThrow(() -> 
                        new UsernameNotFoundException("User Not Found with -> login : " + username)
        );
 
        return UserPrinciple.build(user);
    }
}
