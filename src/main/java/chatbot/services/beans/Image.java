package chatbot.services.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Image implements Serializable{

	private static final long serialVersionUID = 9057173598767349697L;
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id_image;
	private String titre;
	private String url;
	 
	 @OneToMany(cascade=CascadeType.ALL ,fetch= FetchType.EAGER,mappedBy="image")
	  private List<EtapeBranche> etapes= new ArrayList<EtapeBranche>();
	 
	public Image() {
		super();
	}
	public long getId_image() {
		return id_image;
	}
	public void setId_image(long id_image) {
		this.id_image = id_image;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public void setEtapes(List<EtapeBranche> etapes) {
		this.etapes = etapes;
	}
	
	 
	
}
