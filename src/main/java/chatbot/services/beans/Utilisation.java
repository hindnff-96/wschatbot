package chatbot.services.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Utilisation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1699896100697083726L;
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id_utilisation;
	private Date date;
	@ManyToOne
	private Application application;
	
	
	public Utilisation() {
		super();
	}
	public long getId_utilisation() {
		return id_utilisation;
	}
	public void setId_utilisation(long id_utilisation) {
		this.id_utilisation = id_utilisation;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Application getApplication() {
		return application;
	}
	public void setApplication(Application application) {
		this.application = application;
	}
	
	

}
