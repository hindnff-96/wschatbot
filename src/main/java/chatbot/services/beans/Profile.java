package chatbot.services.beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity

public class Profile implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4442403152724618078L;
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id_profil;
	private String profil;
	 
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "Permission_Profile", 
	           joinColumns = { @JoinColumn(name = "id_profil") }, 
	           inverseJoinColumns = { @JoinColumn(name = "id_droit") })
	private List<Permission_to_access> permissions;
	
	
	  @OneToMany(cascade=CascadeType.ALL)
	    @JoinColumn(name="Profile_ID")
	  private List<User> utilisateurs;
	
	  public Profile() {
		super();
	}

	public Long getIdProfil() {
		return id_profil;
	}


	public void setIdProfil(Long idProfil) {
		this.id_profil = idProfil;
	}


	public String getProfil() {
		return profil;
	}


	public void setProfil(String profil) {
		this.profil = profil;
	}
	
	
	
}
