package chatbot.services.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity

public class Branche implements Serializable{

	private static final long serialVersionUID = -3120722086683747589L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id_branche;
	private String titre;
	@Column(columnDefinition = "TEXT")
	private String description;
	private boolean racine;
	private boolean principale;

	private int niveau;
	@ManyToOne
	private Application application;
	
	@OneToMany(cascade = {CascadeType.ALL},fetch= FetchType.EAGER,mappedBy = "superBranche")
    private List<DecisionTree> decisionTree = new ArrayList<DecisionTree>();
	
	/*@OneToMany(cascade = {CascadeType.ALL},fetch= FetchType.EAGER,mappedBy = "sousBranche")
    private Set<DecisionTree> decisionTreeSous;	*/	
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="branche")
	@OrderBy("id_etape")
  private List<EtapeBranche> etapes= new ArrayList<EtapeBranche>();
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="branche")
	private List<MotCle> keywords= new ArrayList<MotCle>();
	
	
	public boolean isPrincipale() {
		return principale;
	}

	public void setPrincipale(boolean principale) {
		this.principale = principale;
	}
	
	public int getNiveau() {
		return niveau;
	}

	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}

	public List<MotCle> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<MotCle> keywords) {
		this.keywords = keywords;
	}

	public List<EtapeBranche> getEtapes() {
		return etapes;
	}

	public void setEtapes(List<EtapeBranche> etapes) {
		this.etapes = etapes;
	}

	public Branche() {
		super();
	}

	public Long getId_branche() {
		return id_branche;
	}

	public void setId_branche(Long id_branche) {
		this.id_branche = id_branche;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isRacine() {
		return racine;
	}

	public void setRacine(boolean racine) {
		this.racine = racine;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public List<DecisionTree> getDecisionTree() {
		return decisionTree;
	}

	public void setDecisionTree(List<DecisionTree> decisionTree) {
		this.decisionTree = decisionTree;
	}

}
