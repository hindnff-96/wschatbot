package chatbot.services.beans;



import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.JoinColumn;


@Entity
public class Permission_to_access implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5535376811223295590L;
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id_droit;
    @Column(unique = true, nullable = false)
	private String droit;
	
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "Permission_Profile", 
               joinColumns = { @JoinColumn(name = "id_droit") }, 
               inverseJoinColumns = { @JoinColumn(name = "id_profil") })
    private List<Profile> profiles;
		
}
