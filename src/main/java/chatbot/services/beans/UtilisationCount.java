package chatbot.services.beans;

public class UtilisationCount {

	private String date;
	private String count;
	
	
	public UtilisationCount() {
		super();
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	
}
