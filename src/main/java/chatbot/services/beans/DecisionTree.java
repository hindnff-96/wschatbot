package chatbot.services.beans;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class DecisionTree implements Serializable{

	private static final long serialVersionUID = -4583648934227100891L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id_super_sous_branche;

    @ManyToOne
    @JoinColumn(name = "super_branche_id")
    private  Branche superBranche;

    
	@ManyToOne
    @JoinColumn(name = "sous_branche_id")
    private Branche sousBranche;

	
	public Branche getSousBranche() {
		return sousBranche;
	}
	
	public void setSousBranche(Branche sousBranche) {
		this.sousBranche = sousBranche;
	}

	public void setSuperBranche(Branche superBranche) {
		this.superBranche = superBranche;
	}	

	@JsonIgnore
	public Branche getSuperBranche() {
		return superBranche;
	}

	public long getId_super_sous_branche() {
		return id_super_sous_branche;
	}

	public void setId_super_sous_branche(long id_super_sous_branche) {
		this.id_super_sous_branche = id_super_sous_branche;
	}

	
	
    
    
}
