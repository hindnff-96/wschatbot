package chatbot.services.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.UniqueConstraint;


@Entity
public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5945999579257296076L;
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id_utilisateur;
	@Column(name = "login", unique = true, nullable = false)	
	private String login;
	@Column(name = "email", unique = true, nullable = false)	
	private String email;
	private String mdp;
	@ManyToOne
	private Profile profil;
	@ManyToOne
	private Application application;

	public User() {
		super();
	}
	
	
	public User(Long id_utilisateur, String login, String email, String mdp, Profile profil, Application application) {
		super();
		this.id_utilisateur = id_utilisateur;
		this.login = login;
		this.email = email;
		this.mdp = mdp;
		this.profil = profil;
		this.application = application;
	}



	public Long getId_utilisateur() {
		return id_utilisateur;
	}
	public void setId_utilisateur(Long id_utilisateur) {
		this.id_utilisateur = id_utilisateur;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public Profile getProfil() {
		return profil;
	}
	public void setProfil(Profile profil) {
		this.profil = profil;
	}
	public Application getApplication() {
		return application;
	}
	public void setApplication(Application application) {
		this.application = application;
	}
	
	public static User create(User user) {
        

        return new User(
                user.getId_utilisateur(),
                user.getLogin(),
                user.getEmail(),
                user.getMdp(),
                user.getProfil(),
                user.getApplication()
        );
    }
	
}
