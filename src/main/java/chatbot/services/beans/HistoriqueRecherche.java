package chatbot.services.beans;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class HistoriqueRecherche implements Serializable{

	private static final long serialVersionUID = -4412164533344047940L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id_historique_recherche;
	private String recherche;
	
	@ManyToOne(optional = false)
	@JoinColumn(name="forApplication")
	private Application application;

	
	public HistoriqueRecherche() {
		super();
	}

	public long getId_historique_recherche() {
		return id_historique_recherche;
	}

	public void setId_historique_recherche(long id_historique_recherche) {
		this.id_historique_recherche = id_historique_recherche;
	}

	public String getRecherche() {
		return recherche;
	}

	public void setRecherche(String recherche) {
		this.recherche = recherche;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}
	
	

}
