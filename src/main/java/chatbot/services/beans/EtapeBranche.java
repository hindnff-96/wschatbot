package chatbot.services.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class EtapeBranche implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7733426203645379110L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id_etape;
	private Long num_etape;
	
	@ManyToOne
	private Image image;
	
	
	public Image getImage() {
		return image;
	}
	public void setImage(Image image) {
		this.image = image;
	}
	public void setBranche(Branche branche) {
		this.branche = branche;
	}
	public Long getNum_etape() {
		return num_etape;
	}
	public void setNum_etape(Long num_etape) {
		this.num_etape = num_etape;
	}
	private String description;
	@ManyToOne
	private Branche branche;
	
	
	public EtapeBranche() {
		super();
	}
	public Long getId_etape() {
		return id_etape;
	}
	public void setId_etape(Long id_etape) {
		this.id_etape = id_etape;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	/*public Branche getBranche() {
		return branche;
	}
	public void setBranche(Branche branche) {
		this.branche = branche;
	}*/
	
	

}
