package chatbot.services.beans;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.lang.NonNull;

@Entity
public class MotCle implements Serializable{
	private static final long serialVersionUID = -7312097483660579622L;
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id_motCle;
	@NonNull
	private String motCle;
	@ManyToOne
	private Branche branche;
	public MotCle() {
		super();
	}
	public long getId_motCle() {
		return id_motCle;
	}
	public void setId_motCle(long id_motCle) {
		this.id_motCle = id_motCle;
	}
	public String getMotCle() {
		return motCle;
	}
	public void setMotCle(String motCle) {
		this.motCle = motCle;
	}
	
	public void setBranche(Branche branche) {
		this.branche = branche;
	}
	
	

}
