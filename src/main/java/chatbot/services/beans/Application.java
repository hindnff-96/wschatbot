package chatbot.services.beans;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity

public class Application implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2310937979563911505L;
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id_application;
	private String name;
	private String description;
	 
	 
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Application() {
		super();
	}

	public Long getId_application() {
		return id_application;
	}

	public void setId_application(Long id_application) {
		this.id_application = id_application;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
}
