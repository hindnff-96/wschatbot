package chatbot.services.beans;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity

public class Historique implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4024659730018947495L;
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id_historique;
	private String action;
	

    private String date;
    
	@ManyToOne(optional = false)
    @JoinColumn(name="created_by")
    private User createdBy;
	@ManyToOne(optional = false)
	@JoinColumn(name="forApplication")
	private Application forApplication;
	
	
	public Application getForApplication() {
		return forApplication;
	}

	public void setForApplication(Application forApplication) {
		this.forApplication = forApplication;
	}

	public Historique() {
		super();
	}

	public Historique(Long id_historique, String action, String date, User createdBy) {
		super();
		this.id_historique = id_historique;
		this.action = action;
		this.date = date;
		this.createdBy = createdBy;
	}

	public Long getId_historique() {
		return id_historique;
	}

	public void setId_historique(Long id_historique) {
		this.id_historique = id_historique;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}
	
	
	
	
}
