package chatbot.services.metier;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import chatbot.services.beans.Branche;
import chatbot.services.dao.BrancheRepository;
import chatbot.services.dao.DecisionTreeRepository;
import chatbot.services.dao.EtapeRepository;
import chatbot.services.dao.MotCleRepository;

@RestController
public class BrancheController {

	@Autowired
	private MotCleRepository motCleRepository;
	@Autowired
	private BrancheRepository brancheRepository;
	
	@Autowired
	private EtapeRepository etapeRepository;
	
	@Autowired
	private DecisionTreeRepository decisionTreeRepository;
	
	@Autowired 
	private DecisionTreeController decisionTreeController;
	
	@GetMapping("chatbot/maxLevel/{id}")
	public String maxLevel(@PathVariable long id) {
		return brancheRepository.maxLevel(id);
	}
	
	@GetMapping("chatbot/statistique/branches")
	public String displayStatsUtilisation(){
		return brancheRepository.countAll();
	}
	
	@GetMapping("chatbot/statistique/branches/{app}")
	public String displayUtilisation(@PathVariable String app){
		return brancheRepository.countByApplication(app);
	}
	
	@PostMapping("/BrunchBykeyword")
	public List<Branche> searchBrunchByKeyword(@RequestBody String jsonObj) throws JSONException {
		 JSONObject jObject = new JSONObject(jsonObj);
		return brancheRepository.findBrunchByKeyword(jObject.get("app").toString(),jObject.get("keyword").toString());
		
	}
	
	@PostMapping("/BrunchBykeywordByApp")
	public List<Branche> searchBrunchByKeywordByApp(@RequestBody String jsonObj) throws JSONException {
		 JSONObject jObject = new JSONObject(jsonObj);
		return brancheRepository.findBrunchByKeywordByAPP(Long.valueOf(jObject.get("app").toString()),jObject.get("keyword").toString());
		
	}
	
	@GetMapping("/arbre/{titre}")
	public Branche tree(@PathVariable String titre) {
			return brancheRepository.findByTitre(titre);
		
	}
	
	@GetMapping("/arbre/branche/{id}")
	public Branche tree(@PathVariable long id) {
			return brancheRepository.findById(id).orElse(null);
		
	}
	
	@GetMapping("/brancheSearch/{titre}")
	public Page<Branche> BrunchSearch(@RequestParam int page,@PathVariable String titre) {
		Pageable pageableRequest = PageRequest.of(page, 12);
			return brancheRepository.findRootsByTitreContains(pageableRequest,titre);
		
	}
	
	@GetMapping("/arbre/princiaples/{app}")
	public List<Branche> principales(@PathVariable String app) {
		return brancheRepository.recupererPrincipales(app);
		
	}
	
	@GetMapping("/arbre/princiaplesById/{id}")
	public List<Branche> principalesById(@PathVariable long id) {
		return brancheRepository.recupererPrincipalesById(id);
		
	}
	
	@GetMapping("/branches/racines/{app}")
	public Page<Branche> racinesByApp(@RequestParam int page, @PathVariable String app) {
		Pageable pageableRequest = PageRequest.of(page, 12);
		return brancheRepository.recupererRacinesByApp(pageableRequest, app);
	}
	
	@GetMapping("/branches")
	public List<Branche> branches() {
		return brancheRepository.findAll();
	}
	
	@SuppressWarnings("deprecation")
	@GetMapping("/branches/racines")
	public Page<Branche> racines(@RequestParam int page) {
		return brancheRepository.recupererRacines(new PageRequest(page, 12));
	}
	
	@GetMapping("/branches/{id}")
	public Branche brancheById(@PathVariable long id) {
		return brancheRepository.findById(id).orElse(null);
	}
	
	@PostMapping("/branches")
	public ResponseEntity<Object> ajouterBranche(@RequestBody Branche p) {
		//p.setNiveau(1);
		Branche savedBrunch = brancheRepository.save(p);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedBrunch.getId_branche()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@DeleteMapping("/branches/{id}")
	public void suppapplication(@PathVariable String id) {
		decisionTreeController.findTree(id);
	}
	
	@PutMapping("/branches/{id}")
	public ResponseEntity<Object> updateApp(@RequestBody Branche p, @PathVariable long id) {
		p.setEtapes(etapeRepository.findStepsByBrunch(id));
		p.setDecisionTree(decisionTreeRepository.findTree(id));
		p.setKeywords(motCleRepository.findKeywordsByBrunch(id));
		Optional<Branche> braunchOptional = brancheRepository.findById(id);
		if (!braunchOptional.isPresent()) 
			return ResponseEntity.notFound().build();
		p.setId_branche(id);
		brancheRepository.save(p);
		return ResponseEntity.noContent().build();
	}

	@GetMapping("/branches/application/{app}")
	public List<Branche> branchesByApp(@PathVariable String app) {
		return brancheRepository.recupererBranchesByApp(app);
	}
	
	@GetMapping("/chatbot/search/{app}/{keyword}")
	public List<Branche> searchChatbot(@PathVariable String keyword, @PathVariable String app) {
		return brancheRepository.findByTitreOrDescriptionContains(keyword,app);
	}
	
	@GetMapping("/chatbot/search/{app}/id/{id}")
	public Branche searchChatbotByID(@PathVariable long id, @PathVariable String app) {
		return brancheRepository.findAppBrunchById(id,app);
	}
	
	@GetMapping("/chatbot/search/app/{app}/id/{id}")
	public Branche searchChatbotByIDApp(@PathVariable long id, @PathVariable long app) {
		return brancheRepository.findAppIDBrunchById(id, app);
	}
	
	@GetMapping("/chatbot/searchRoot/{app}/{keyword}/{niveau}")
	public List<Branche> searchRootChatbot(@PathVariable String keyword, @PathVariable String app, @PathVariable int niveau) {
		return brancheRepository.findRootsByLevel(keyword,app, niveau);
	}
	
	@GetMapping("/chatbot/searchRootByID/{app}/{keyword}/{niveau}")
	public List<Branche> searchRootChatbotByID(@PathVariable String keyword, @PathVariable long app, @PathVariable int niveau) {
		return brancheRepository.findRootsByLevelByApp(keyword,app, niveau);
	}
	
}
