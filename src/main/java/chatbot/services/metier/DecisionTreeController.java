package chatbot.services.metier;

import java.net.URI;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import chatbot.services.beans.Branche;
import chatbot.services.beans.DecisionTree;
import chatbot.services.dao.BrancheRepository;
import chatbot.services.dao.DecisionTreeRepository;

@RestController
public class DecisionTreeController {

	@Autowired
	private DecisionTreeRepository decisionTreeRepository;
	
	@Autowired
	private BrancheRepository brancheRepository;
	
	@PostMapping("/decisionTree/add/{idSuperBr}")
	public ResponseEntity<Object> ajouterLigne(@RequestBody DecisionTree ligne, @PathVariable Long idSuperBr) {
		Branche superBr=brancheRepository.findById(idSuperBr).orElse(null);
		ligne.setSuperBranche(superBr);
		ligne.getSousBranche().setNiveau(ligne.getSuperBranche().getNiveau()+1);
		Branche br=brancheRepository.saveAndFlush(ligne.getSousBranche());
		System.out.println(br.getNiveau());
		/*Branche superB=ligne.getSuperBranche();
		int niveau=superB.getNiveau();
		superB.setNiveau(niveau);
		brancheRepository.saveAndFlush(superB);*/
		ligne.setSousBranche(br);
		DecisionTree savedLigne= decisionTreeRepository.save(ligne);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedLigne.getId_super_sous_branche()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PostMapping("/decisionTree/exist")
	public ResponseEntity<Object> ajouterLigneExist(@RequestBody DecisionTree ligne) {

		DecisionTree savedLigne= decisionTreeRepository.save(ligne);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedLigne.getId_super_sous_branche()).toUri();
		return ResponseEntity.created(location).build();
	}

	@DeleteMapping("/decisionTree/{id}")
	public void findTree(@PathVariable String id){
		ArrayList<DecisionTree> tree=decisionTreeRepository.findTree(Long.valueOf(id));
		for(int i=0;i<tree.size();i++) {		
			decisionTreeRepository.supprimer(tree.get(i).getId_super_sous_branche());		
		}
		brancheRepository.deleteById(Long.valueOf(id));

	}
	
	@DeleteMapping("/decisionTree/removeLink/{id}")
	public void deleteLink(@PathVariable String id){
		decisionTreeRepository.supprimer(Long.valueOf(id));

	}
	
	@GetMapping("/decisionTree/{id}")
	public DecisionTree findRelationById(@PathVariable String id){
		return decisionTreeRepository.findById(Long.valueOf(id)).orElse(null);
	}
	
}
