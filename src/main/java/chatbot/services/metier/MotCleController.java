package chatbot.services.metier;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import chatbot.services.beans.Branche;
import chatbot.services.beans.MotCle;
import chatbot.services.dao.MotCleRepository;

@RestController
public class MotCleController {

	@Autowired
	private MotCleRepository motCleRepositoty;
	
	
	
	@GetMapping("/keyword/{app}")
	public List<MotCle> searchKeyword(@PathVariable String app) {
		return motCleRepositoty.findKeywordsByApp(app);
		
	}
	
	@GetMapping("/keywordById/{id}")
	public List<MotCle> searchKeywordById(@PathVariable long id) {
		return motCleRepositoty.findKeywordsByAppById(id);
		
	}
	
	@PostMapping("/keyword")
	public ResponseEntity<Object> ajouterKeyWord(@RequestBody MotCle p) {
		System.out.println(p.getMotCle());
		MotCle savedKeyWord = motCleRepositoty.save(p);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedKeyWord.getId_motCle()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@DeleteMapping("/keyword/{id}")
	public void suppapplication(@PathVariable String id) {
		motCleRepositoty.deleteById(Long.valueOf(id));
	}
	
	@PutMapping("/keyword/{id}")
	public ResponseEntity<Object> updateApp(@RequestBody MotCle p, @PathVariable long id) {
		Optional<MotCle> savedKeyWord = motCleRepositoty.findById(id);
		if (!savedKeyWord.isPresent())
			return ResponseEntity.notFound().build();
		p.setId_motCle(id);
		motCleRepositoty.save(p);
		return ResponseEntity.noContent().build();
	}
}
