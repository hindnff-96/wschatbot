package chatbot.services.metier;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import chatbot.services.beans.HistoriqueRecherche;
import chatbot.services.beans.UtilisationCount;
import chatbot.services.dao.HistoriqueRechercheRepository;

@RestController
public class HistoriqueRechercheController {

	@Autowired
	HistoriqueRechercheRepository historiqueRechercheRepository;
	
	@SuppressWarnings("deprecation")
	@GetMapping("/chatbot/history")
	public Page<HistoriqueRecherche> historique(@RequestParam int page) {
		return historiqueRechercheRepository.findAllDesc(new PageRequest(page, 6));
	}
	
	@GetMapping("chatbot/statistique/recherches")
	public String displayStatsUtilisation(){
		return historiqueRechercheRepository.countAll();
	}
	
	@GetMapping("chatbot/statistique/recherches/{app}")
	public String displayUtilisation(@PathVariable String app){
		return historiqueRechercheRepository.countByApplication(app);
	}
	
	@GetMapping("chatbot/history/application/{app}")
	public Page<HistoriqueRecherche> historiqueByApp(@PathVariable String app,@RequestParam int page) {
		Pageable pageableRequest = PageRequest.of(page, 6);
		return historiqueRechercheRepository.findByApplication(pageableRequest,app);
	}
	
	@GetMapping("/chatbot/historique")
	public List<HistoriqueRecherche> listeRecherche(){
		return historiqueRechercheRepository.findAll();
	}
	
	@GetMapping("/chatbot/historique/{app}")
	public List<HistoriqueRecherche> listeRechercheApp(String app){
		return historiqueRechercheRepository.findfByApplication(app);
	}
	
	@PostMapping("/chatbot/historique")
	public void ajouterHist(@RequestBody HistoriqueRecherche hist) {
		if(historiqueRechercheRepository.findByRecherche(hist.getRecherche())==null) {
			HistoriqueRecherche savedHist = historiqueRechercheRepository.save(hist);
			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(savedHist.getId_historique_recherche()).toUri();
			ResponseEntity.created(location).build();
		}
		
	}
	
	
}
