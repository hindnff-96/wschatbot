package chatbot.services.metier;

import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import chatbot.services.beans.Historique;
import chatbot.services.dao.HistoryRepository;

@RestController
public class HistoryController {

	@Autowired
	private HistoryRepository historyRepository;
	
	@SuppressWarnings("deprecation")
	@GetMapping("/history")
	public Page<Historique> historique(@RequestParam int page) {
		return historyRepository.findAllDesc(new PageRequest(page, 6));
	}
	
	@GetMapping("/history/{login}")
	public Page<Historique> historiqueByUser(@PathVariable String login, @RequestParam int page) {
		Pageable pageableRequest = PageRequest.of(page, 6);
		return historyRepository.findByCreatedBy(pageableRequest,login);
	}
	
	@GetMapping("/history/{login}/{app}")
	public Page<Historique> historiqueByUserApp(@PathVariable String login,@PathVariable String app,@RequestParam int page) {
		Pageable pageableRequest = PageRequest.of(page, 6);
		return historyRepository.findByUserApplication(pageableRequest,app);
	}
	
	@PostMapping("/history")
	public ResponseEntity<Object> ajouterHistorique(@RequestBody Historique p) {
		Historique savedHistory = historyRepository.save(p);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedHistory.getId_historique()).toUri();
		return ResponseEntity.created(location).build();
	}

	@GetMapping("/history/application/{app}")
	public Page<Historique> historiqueByApp(@PathVariable String app,@RequestParam int page) {
		Pageable pageableRequest = PageRequest.of(page, 6);
		return historyRepository.findAllDescBpp(pageableRequest,app);
	}
}
