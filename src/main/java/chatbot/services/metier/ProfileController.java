package chatbot.services.metier;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import chatbot.services.beans.Profile;
import chatbot.services.dao.ProfileRepository;

@RestController
public class ProfileController {
	@Autowired
	private ProfileRepository profilRepository;

	@GetMapping("/profils")
	public List<Profile> profils() {
		return profilRepository.findAll();
	}
	
	@GetMapping("/droits/{id}")
	public List<String> droitsProfil(@PathVariable long id) {
		return profilRepository.findPermissionsById(id);
	}
	
	@GetMapping("/profils/{id}")
	public Profile profil(@PathVariable long id) {
		return profilRepository.findById(id).orElse(null);
	}
	
	@PostMapping("/profils")
	public ResponseEntity<Object> ajouterProfil(@RequestBody Profile p) {
		Profile savedprofile = profilRepository.save(p);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedprofile.getIdProfil()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@DeleteMapping("/profils/{id}")
	public void suppProfil(@PathVariable long id) {
		profilRepository.deleteById(id);
	}
	
	@PutMapping("/profils/{id}")
	public ResponseEntity<Object> updateStudent(@RequestBody Profile p, @PathVariable long id) {
		Optional<Profile> profilOptional = profilRepository.findById(id);
		if (!profilOptional.isPresent())
			return ResponseEntity.notFound().build();
		p.setIdProfil(id);
		profilRepository.save(p);
		return ResponseEntity.noContent().build();
	}
}
