package chatbot.services.metier;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import chatbot.services.beans.EtapeBranche;
import chatbot.services.beans.Image;
import chatbot.services.dao.EtapeRepository;
import chatbot.services.dao.ImageRepository;

@RestController
public class EtapeController {

	@Autowired
	private EtapeRepository etapeRepository;
	
	@Autowired
	private ImageController imageController;
	
	@Autowired
	private ImageRepository imageRepository;

	@GetMapping("etape/branche/{id}")
	public List<EtapeBranche> getBruchSteps(@PathVariable long id){
		return etapeRepository.findStepsByBrunch(id);
	}
	@GetMapping("etape")
	public List<EtapeBranche> getSteps(){
		return etapeRepository.findAll();
	}
	
	@PostMapping("/etape")
	public ResponseEntity<Object> ajouterHistorique(@RequestBody EtapeBranche p) throws IOException {
		if(p.getImage()!=null) {
			Image saverImage= imageController.addImage(p.getImage());
			p.setImage(saverImage);
		}
		EtapeBranche savetStep = etapeRepository.save(p);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savetStep.getId_etape()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@DeleteMapping("/etape/{id}")
	public void suppEtape(@PathVariable String id) {
		Image image=etapeRepository.findById(Long.valueOf(id)).orElseGet(null).getImage();
		if(image==null) {
			etapeRepository.deleteById(Long.valueOf(id));
		}else {
			imageRepository.deleteById(image.getId_image());
		}
		
	}
	
	@PutMapping("/etape/{id}")
	public ResponseEntity<Object> updateApp(@RequestBody EtapeBranche p, @PathVariable long id) throws IOException {
		Optional<EtapeBranche> stepOptional = etapeRepository.findById(id);
		if (!stepOptional.isPresent())
			return ResponseEntity.notFound().build();
		p.setId_etape(id);
		//EtapeBranche etape=etapeRepository.findById(id).orElse(null);
		//System.out.pr
		if(p.getImage()!=null) {
			Image saverImage= imageController.addImage(p.getImage());
			p.setImage(saverImage);
		}
		etapeRepository.save(p);
		//imageRepository.deleteById(etape.getImage().getId_image());
		return ResponseEntity.noContent().build();
	}
}
