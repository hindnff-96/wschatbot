package chatbot.services.metier;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Optional;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import chatbot.services.beans.Image;
import chatbot.services.dao.ImageRepository;

@RestController
public class ImageController {
	
	@Autowired
	ImageRepository imageRepositoty;

	public Image addImage(Image i) throws IOException {
		Image savedPic = imageRepositoty.save(i);
		String imagesFolderURL="C:/Users/user/eclipse-workspace/BackOfficeChatbot/src/main/webapp/resources/imagesChatbot/";
		String titre=savedPic.getTitre();
		String url =savedPic.getUrl();
		System.out.println(savedPic.getId_image());
		savedPic.setTitre(titre+"_"+i.getId_image()+".jpg");
		File images = new File(imagesFolderURL+savedPic.getTitre()); 
		File image = new File(url);
		savedPic.setUrl(imagesFolderURL);
		BufferedImage imageBuffered = ImageIO.read(image);
		//ByteArrayOutputStream out = new ByteArrayOutputStream();
		ImageIO.write(imageBuffered, "jpg", images);
		savedPic.setUrl(imagesFolderURL);
		updatePic(savedPic, savedPic.getId_image());
		return savedPic;
	}
	
	@PostMapping("/image")
	public ResponseEntity<Object> ajouterImage(@RequestBody Image i) throws IOException {
		
		Image savedPic = imageRepositoty.save(i);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedPic.getId_image()).toUri();
		String imagesFolderURL="C:/Users/user/eclipse-workspace/BackOfficeChatbot/src/main/webapp/resources/imagesChatbot/";
		String titre=savedPic.getTitre();
		String url =savedPic.getUrl();
		System.out.println(savedPic.getId_image());
		savedPic.setTitre(titre+"_"+i.getId_image()+".jpg");
		File images = new File(imagesFolderURL+savedPic.getTitre()); 
		File image = new File(url);
		savedPic.setUrl(imagesFolderURL);
		BufferedImage imageBuffered = ImageIO.read(image);
		//ByteArrayOutputStream out = new ByteArrayOutputStream();
		ImageIO.write(imageBuffered, "jpg", images);
		savedPic.setUrl(imagesFolderURL);
		updatePic(savedPic, savedPic.getId_image());
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping("/image/{id}")
	public ResponseEntity<Object> updatePic(@RequestBody Image i, @PathVariable long id) throws IOException {
		Optional<Image> picOptional = imageRepositoty.findById(id);
		if (!picOptional.isPresent())
			return ResponseEntity.notFound().build();
		i.setId_image(id);
		imageRepositoty.save(i);		
		
		return ResponseEntity.noContent().build();
	}
	
	@DeleteMapping("/image/{id}")
	public void suppImage(@PathVariable long id) {
		imageRepositoty.deleteById(id);
	}
}
