package chatbot.services.metier;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import chatbot.services.beans.User;
import chatbot.services.dao.UserRepository;

@RestController
public class UserController {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
    PasswordEncoder encoder;
	
	@GetMapping(value= {"/users","/userSearch"})
	public List<User> users() {
		return userRepository.findAll();
	}
	
	@GetMapping(value= {"/users/application/{app}","/userSearch/application/{app}"})
	public List<User> usersApp(@PathVariable String app) {
		return userRepository.findByApplication(app);
	}
	
	@GetMapping("/user/{login}")
	public User User(@PathVariable String login) {
		return userRepository.findByLogin(login).orElse(null);
	}
	
	@GetMapping("/userSearch/{login}")
	public List<User> UserSearch(@PathVariable String login) {
			return userRepository.findByLoginContains(login);
		
	}
	
	@PostMapping("/users")
	public ResponseEntity<Object> ajouterUser(@RequestBody User p) {
		String mdp=encoder.encode(p.getMdp()).toString();
		p.setMdp(mdp);
		User savedUser = userRepository.save(p);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedUser.getId_utilisateur()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@DeleteMapping("/users/{login}")
	public void suppUser(@PathVariable String login) {
		userRepository.deleteByLogin(login);
	}
	
	@PutMapping("/users/{id}")
	public ResponseEntity<Object> updateUtilisateur(@RequestBody User p, @PathVariable long id) {
		Optional<User> UserOptional = userRepository.findById(id);
		if (!UserOptional.isPresent())
			return ResponseEntity.notFound().build();
		p.setId_utilisateur(id);
		userRepository.save(p);
		return ResponseEntity.noContent().build();
	}
	
	

}
