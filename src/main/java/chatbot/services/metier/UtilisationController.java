package chatbot.services.metier;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import chatbot.services.beans.Utilisation;
import chatbot.services.beans.UtilisationCount;
import chatbot.services.dao.ApplicationRepository;
import chatbot.services.dao.UtilisationRepository;

@RestController
public class UtilisationController {

	@Autowired
	UtilisationRepository utilisationRepository;
	
	@Autowired 
	ApplicationRepository applicationRepository;
	
	@GetMapping("chatbot/utilisation")
	public List<UtilisationCount> displayUtilisation(){
		List<Object[]> utilisations=utilisationRepository.countAll();
		/*HashMap<String, String> map = new HashMap<>();
		String json="{ \n \t \"utilisations\": [";
		
		for (Object[] result : utilisations) {	
			json+="\n \t \t {\n \t \t \"date\":\""+(String) result[1]+"\",\n \t \t \"count\":\""+String.valueOf(((Number) result[0]).intValue())+"\" \n \t \t},\n";
		}
		
		json=json.substring(0, json.length()-2);
		json+="\n \t ] \n }";*/
		//System.out.println(json);
		
		/*for (Object[] result : utilisations) {	  
			
		    map.put("date", (String) result[1]);
		    map.put("count", String.valueOf(((Number) result[0]).intValue()));
		}
		
		

	    */
		//return json;
		
		List<UtilisationCount> counts = new ArrayList<UtilisationCount>();
		for (Object[] result : utilisations){
			UtilisationCount count =new UtilisationCount();
			count.setCount(String.valueOf(((Number) result[0]).intValue()));
			count.setDate(((Date) result[1]).toString());
			counts.add(count);
		}
		return counts;
	}
	
	@GetMapping("chatbot/utilisation/{app}")
	public List<UtilisationCount> displayUtilisation(@PathVariable String app){
		List<Object[]> utilisations=utilisationRepository.countByApplication(app);
		List<UtilisationCount> counts = new ArrayList<UtilisationCount>();
		for (Object[] result : utilisations){
			UtilisationCount count =new UtilisationCount();
			count.setCount(String.valueOf(((Number) result[0]).intValue()));
			count.setDate(((Date) result[1]).toString());
			counts.add(count);
		}
		return counts;
	}
	
	
	
	@GetMapping("chatbot/applicationUse")
	public List<UtilisationCount> displayUtilisationApps(){
		List<Object[]> utilisations=utilisationRepository.countAppUses();
		int somme=0;
		for (Object[] result : utilisations){
			somme+=((Number) result[0]).intValue();
		}
		List<UtilisationCount> counts = new ArrayList<UtilisationCount>();
		for (Object[] result : utilisations){
			UtilisationCount count =new UtilisationCount();
			float pourcentage=(Float.valueOf(result[0].toString())/somme)*100;
			count.setCount(String.valueOf(pourcentage));
			count.setDate((String) result[1]);
			counts.add(count);
		}
		System.out.println(somme);
		return counts;
	}
	@PostMapping("chatbot/utilisation")
	public ResponseEntity<Object> ajouterDemande(@RequestBody String app) throws ParseException {
		LocalDate myObj = LocalDate.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd"); 
		String formattedDate = myObj.format(myFormatObj); 
		Utilisation utilisation=new Utilisation();
		utilisation.setApplication(applicationRepository.findByName(app));
		utilisation.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(formattedDate));
		Utilisation savedDemande = utilisationRepository.save(utilisation);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedDemande.getId_utilisation()).toUri();
		return ResponseEntity.created(location).build();
	}
}
