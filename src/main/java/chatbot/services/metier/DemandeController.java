package chatbot.services.metier;

import java.net.URI;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import chatbot.services.beans.Demande;
import chatbot.services.dao.ApplicationRepository;
import chatbot.services.dao.DemandeRepository;
@RestController
public class DemandeController {
	@Autowired
	private DemandeRepository demandeRepository;
	@Autowired
	private ApplicationRepository applicationRepository;
	
	
	@GetMapping("chatbot/statistique/demandes")
	public String displayStatsUtilisation(){
		return demandeRepository.countAll();
	}
	
	@GetMapping("chatbot/statistique/demandes/{app}")
	public String displayUtilisation(@PathVariable String app){
		return demandeRepository.countByApplication(app);
	}
	
	@SuppressWarnings("deprecation")
	@GetMapping("/demandes")
	public Page<Demande> demande(@RequestParam int page) {
		return demandeRepository.findAllDesc(new PageRequest(page, 6));
	}
	
	@GetMapping("/demandes/{id}")
	public Demande demandeByDemande(@PathVariable long id) {
		return demandeRepository.findById(id).orElse(null);
	}
	
	@SuppressWarnings("deprecation")
	@GetMapping("/demandes/application/{app}")
	public Page<Demande> demandeByApp(@PathVariable String app, @RequestParam int page) {
		
		return demandeRepository.findByApplication(new PageRequest(page, 6),app);
	}
	
	@PostMapping("/demandes")
	public ResponseEntity<Object> ajouterDemande(@RequestBody Demande p) {
		Demande savedDemande = demandeRepository.save(p);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedDemande.getId_demande()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping("/demandes/{id}")
	public ResponseEntity<Object> updateUtilisateur(@RequestBody Demande p, @PathVariable long id) {
		Optional<Demande> demandeOptional = demandeRepository.findById(id);
		if (!demandeOptional.isPresent())
			return ResponseEntity.notFound().build();
		p.setId_demande(id);
		demandeRepository.save(p);
		return ResponseEntity.noContent().build();
	}
	
	@PostMapping("/demandes/{app}")
	public ResponseEntity<Object> ajouterDemandeByApp(@RequestBody Demande p, @PathVariable String app) {
		p.setApplication(applicationRepository.findByName(app));
		Demande savedDemande = demandeRepository.save(p);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedDemande.getId_demande()).toUri();
		return ResponseEntity.created(location).build();
	}
	
}
