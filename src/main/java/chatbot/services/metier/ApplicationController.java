package chatbot.services.metier;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import chatbot.services.beans.Application;
import chatbot.services.dao.ApplicationRepository;

@RestController
public class ApplicationController {
	@Autowired
	private ApplicationRepository applicationRepository;

	@GetMapping(value= {"/applications","/applicationSearch"})
	public List<Application> applications() {
		return applicationRepository.findAll();
	}
	
	@GetMapping("/applications/{id}")
	public Application applications(@PathVariable long id) {
		return applicationRepository.findById(id).orElse(null);
	}
	
	@GetMapping("/applications/notAdministration")
	public List<Application> applicationsNotAdministration() {
		return applicationRepository.findNotEqualsAdministration("Administration");
	}
	
	@GetMapping("/applicationSearch/{app}")
	public List<Application> applicationSearch(@PathVariable String app) {
			return applicationRepository.findByNameContains(app);
		
	}
	
	@GetMapping("/application/{name}")
	public Application Application(@PathVariable String name) {
		return applicationRepository.findByName(name);
	}
	
	@GetMapping("/application/id/{id}")
	public Application Application(@PathVariable long id) {
		return applicationRepository.findById(id).orElse(null);
	}
	
	@PostMapping("/applications")
	public ResponseEntity<Object> ajouterapplication(@RequestBody Application p) {
		Application savedapplication = applicationRepository.save(p);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedapplication.getId_application()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@DeleteMapping("/applications/{app}")
	public void suppapplication(@PathVariable String app) {
		applicationRepository.deleteByName(app);
	}
	
	@PutMapping("/applications/{id}")
	public ResponseEntity<Object> updateApp(@RequestBody Application p, @PathVariable long id) {
		Optional<Application> applicationOptional = applicationRepository.findById(id);
		if (!applicationOptional.isPresent())
			return ResponseEntity.notFound().build();
		p.setId_application(id);
		applicationRepository.save(p);
		return ResponseEntity.noContent().build();
	}
}
