package chatbot.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()

public class TealChatbotServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(TealChatbotServicesApplication.class, args);
	}

}
