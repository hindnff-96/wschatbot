package chatbot.services.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import chatbot.services.beans.Utilisation;

@Repository
public interface UtilisationRepository extends JpaRepository<Utilisation, Long>{

	@Query("select count(u), u.date from Utilisation u, Application a where u.application = a.id_application and a.name=?1 group by u.date order by u.date")
	public List<Object[]> countByApplication(String app);
	
	@Query("select count(u), u.date from Utilisation u group by u.date order by u.date")
	public List<Object[]> countAll();
	
	@Query("select count(u), a.name from Utilisation u, Application a where u.application = a.id_application group by a.name")
	public List<Object[]> countAppUses();
}
