package chatbot.services.dao;

import java.util.List;

import org.hibernate.annotations.FetchProfile.FetchOverride;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import chatbot.services.beans.EtapeBranche;

@Repository
public interface EtapeRepository extends JpaRepository<EtapeBranche, Long>{
	
	@Query("select e from EtapeBranche e , Branche b where e.branche=b.id_branche and b.id_branche=?1 order by e.num_etape ASC")
	public List<EtapeBranche> findStepsByBrunch(long id);
	
	

}
