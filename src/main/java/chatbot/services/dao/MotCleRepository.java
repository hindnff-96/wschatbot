package chatbot.services.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import chatbot.services.beans.Branche;
import chatbot.services.beans.MotCle;

@Repository
public interface MotCleRepository extends JpaRepository<MotCle, Long>{

	@Query("select m from MotCle m , Branche b where m.branche=b.id_branche and b.id_branche=?1")
	public List<MotCle> findKeywordsByBrunch(long id);
	
	@Query("select m from MotCle m , Branche b,Application a where m.branche=b.id_branche and b.application =a.id_application and a.name=?1")
	public List<MotCle> findKeywordsByApp(String app);
	
	@Query("select m from MotCle m , Branche b,Application a where m.branche=b.id_branche and b.application =a.id_application and a.id_application=?1")
	public List<MotCle> findKeywordsByAppById(long id);
	
	
}
