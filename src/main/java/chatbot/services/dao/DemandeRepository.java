package chatbot.services.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import chatbot.services.beans.Demande;


@Repository
public interface DemandeRepository extends JpaRepository<Demande, Long>{

	public long count();
	
	
	@Query("select d from Demande d, Application a where d.application = a.id_application and a.name=?1 order by d.id_demande DESC")
	public Page<Demande> findByApplication(Pageable pageRequest,String app);
	
	@Query("select d from Demande d order by d.id_demande DESC")
	public Page<Demande> findAllDesc(Pageable pageRequest);
	
	@Query("select count(d) from Demande d")
	public String countAll();
	
	@Query("select count(d) from Demande d, Application a where d.application = a.id_application and a.name=?1")
	public String countByApplication(String app);
}
