package chatbot.services.dao;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import chatbot.services.beans.HistoriqueRecherche;

@Repository
public interface HistoriqueRechercheRepository extends JpaRepository<HistoriqueRecherche, Long>{

	@Query("select h from HistoriqueRecherche h, Application a where h.application = a.id_application and a.name=?1 order by h.id_historique_recherche DESC")
	public Page<HistoriqueRecherche> findByApplication(Pageable pageRequest,String app);
	
	@Query("select h from HistoriqueRecherche h order by h.id_historique_recherche DESC")
	public Page<HistoriqueRecherche> findAllDesc(PageRequest pageRequest);
	
	@Query("select h from HistoriqueRecherche h, Application a where h.application=a.id_application and a.name=?1")
	public List<HistoriqueRecherche> findfByApplication(String application);
	
	public HistoriqueRecherche findByRecherche(String keyword);
	
	@Query("select count(h) from HistoriqueRecherche h")
	public String countAll();
	
	@Query("select count(h) from HistoriqueRecherche h, Application a where h.application = a.id_application and a.name=?1")
	public String countByApplication(String app);
}
