package chatbot.services.dao;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import chatbot.services.beans.Application;
import chatbot.services.beans.User;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long>{
	 @Modifying
	    @Transactional
	    @Query("delete from Application u where name = ?1")

	public int deleteByName(String name);
	 
	 
	 public Application findByName(String name);
	 
		public List<Application> findByNameContains(String name);
		 
		@Query("select a from Application a where a.name<> ?1")
			public List<Application> findNotEqualsAdministration(String app);
	    

}
