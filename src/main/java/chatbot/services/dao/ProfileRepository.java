package chatbot.services.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import chatbot.services.beans.Profile;
import chatbot.services.beans.ProfileName;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long>{

	 Optional<Profile> findByProfil(ProfileName profil);
	
    @Query("select pa.droit from Permission_to_access pa, Profile p join p.permissions per where per.id_droit=pa.id_droit and p.id_profil=?1")
	public List<String> findPermissionsById(long id);
	    

}
