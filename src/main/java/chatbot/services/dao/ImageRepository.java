package chatbot.services.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import chatbot.services.beans.Image;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long>{

	
	
}
