package chatbot.services.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import chatbot.services.beans.Historique;

@Repository
public interface HistoryRepository extends JpaRepository<Historique, Long>{

	@Query("select h from Historique h order by h.id_historique DESC")
	 public Page<Historique> findByCreatedBy(Pageable pageableRequest,String login);

	@Query("select h from Historique h, Application a where h.forApplication = a.id_application and a.name=?1 order by h.id_historique DESC")
	public Page<Historique> findByApplication(Pageable pageRequest,String app);
	
	@Query("select h from Historique h, Application a where h.forApplication = a.id_application and a.name=?1 order by h.id_historique DESC")
	public Page<Historique> findByUserApplication(Pageable pageRequest,String app);

	@Query("select h from Historique h order by h.id_historique DESC")
	public Page<Historique> findAllDesc(PageRequest pageRequest);
	
	@Query("select h from Historique h,Application a where h.forApplication = a.id_application and a.name=?1 order by h.id_historique DESC")
	public Page<Historique> findAllDescBpp(Pageable pageRequest,String app);
}
