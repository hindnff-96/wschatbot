package chatbot.services.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import chatbot.services.beans.DecisionTree;

@Transactional
@Repository
public interface DecisionTreeRepository extends JpaRepository<DecisionTree, Long>{

	@Query("select d from DecisionTree d, Branche b where (d.superBranche=b.id_branche or d.sousBranche=b.id_branche) and b.id_branche=?1")
	public ArrayList<DecisionTree> findTree(long id);
	
	@Modifying
	@Query("delete from DecisionTree where id_super_sous_branche=?1")
	public void supprimer(long id);
	
	@Query("select d from DecisionTree d where d.sousBranche=?1 and d.superBranche=?2")
	public DecisionTree find(String branche, String superBranche);
}
