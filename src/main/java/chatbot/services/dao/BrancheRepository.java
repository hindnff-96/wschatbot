package chatbot.services.dao;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import chatbot.services.beans.Branche;

import org.springframework.data.domain.Pageable;

public interface BrancheRepository extends JpaRepository<Branche, Long>{

	@Query("select b from Branche b where racine=true and b.titre like %?1% order by id_branche DESC")
	public Page<Branche> findRootsByTitreContains(Pageable pageRequest,String titre);
	
	@Modifying
    @Transactional
    @Query("delete from Branche u where titre = ?1")
	void deleteByTitre(String branche);

	@Query("from Branche b where racine=true order by id_branche DESC")
	Page<Branche> recupererRacines(PageRequest pageRequest);
	
	@Query("select b from Branche b, Application a where racine=true and b.application=a.id_application and a.name=?1 order by id_branche DESC")
	Page<Branche> recupererRacinesByApp(Pageable pageRequest, String app);

	Branche findByTitre(String racine);
	
	@Query("select b from Branche b, Application a where racine=false and b.application=a.id_application and a.name=?1")
	List<Branche> recupererBranchesByApp(String app);
	
	@Query("select distinct b from Branche b where (b.titre like %?1% or b.description like %?1%) and (b.application.name=?2) and b.description!= '' ")
	public List<Branche> findByTitreOrDescriptionContains(String titre, String app);

	@Query("select b from Branche b where (b.titre like %?1% or b.description like %?1%) and (b.application.name=?2) and b.description!= '' and b.niveau=?3")
	public List<Branche> findRootsByLevel(String titre, String app, int niveau);
	
	@Query("select b from Branche b where (b.id_branche=?1) and (b.application.name=?2)")
	public Branche findAppBrunchById(long id, String app);
	
	@Query("select b from Branche b where (b.id_branche=?1) and (b.application.id_application=?2)")
	public Branche findAppIDBrunchById(long id, long app);
	
	@Query("select b from Branche b, Application a where principale=true and b.application=a.id_application and a.name=?1")
	List<Branche> recupererPrincipales(String app);
	
	@Query("select b from MotCle m , Branche b,Application a where m.branche=b.id_branche and m.motCle like %?2% and b.application =a.id_application and a.name=?1")
	public List<Branche> findBrunchByKeyword(String app, String keyword);
	
	@Query("select count(b) from Branche b")
	public String countAll();
	
	@Query("select count(b) from Branche b, Application a where b.application = a.id_application and a.name=?1")
	public String countByApplication(String app);
	
	@Query("select b from Branche b, Application a where principale=true and b.application=a.id_application and a.id_application=?1")
	List<Branche> recupererPrincipalesById(Long app);
	
	@Query("select b from MotCle m , Branche b,Application a where m.branche=b.id_branche and m.motCle like %?2% and b.application =a.id_application and a.id_application=?1")
	public List<Branche> findBrunchByKeywordByAPP(long app, String keyword);

	@Query("select b from Branche b where (b.titre like %?1% or b.description like %?1%) and (b.application.id_application=?2) and b.description!= '' and b.niveau=?3")
	public List<Branche> findRootsByLevelByApp(String keyword, long app, int niveau);
	
	@Query("select max(niveau) from Branche b where b.application.id_application=?1 ")
	public String maxLevel(long id); 
}
