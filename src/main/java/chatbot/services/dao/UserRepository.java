package chatbot.services.dao;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import chatbot.services.beans.User;
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	public Optional<User> findByLogin(String login);
	//public User findByLogin(String login);
    Boolean existsByLogin(String login);
    Boolean existsByEmail(String email);
    
	User findByEmail(String email);
	
	@Modifying
	    @Transactional
	    @Query("delete from User u where login = ?1")

	public int deleteByLogin(String login);
	 
	 
	 
		public List<User> findByLoginContains(String login);

		 @Query("select u from User u, Application a where u.application = a.id_application and a.name=?1")
		public List<User> findByApplication(String app);
		 
		 @Query("select t from User t where t.email = ?1 OR t.login = ?1")
	User findByLoginOrEmail(String loginOrEmail);
		 
		
}
